package demo.example.DemoSpringBoot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.example.DemoSpringBoot.entity.USERS;
import demo.example.DemoSpringBoot.repository.UserRepository;
import demo.example.DemoSpringBoot.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public List<USERS> getUser() {
		return userRepository.findAll();
	}

	@Override
	public USERS addUser(USERS u) {
		userRepository.save(u);
		return userRepository.getOne(u.getId());
	}

	@Override
	public void delete(String id) {
		userRepository.deleteById(id);
		
	}
	
}
