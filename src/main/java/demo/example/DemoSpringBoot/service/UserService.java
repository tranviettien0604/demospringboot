package demo.example.DemoSpringBoot.service;

import java.util.List;

import demo.example.DemoSpringBoot.entity.USERS;

public interface UserService {
  List<USERS> getUser();
  USERS addUser(USERS u);
  void delete(String id); 
}
