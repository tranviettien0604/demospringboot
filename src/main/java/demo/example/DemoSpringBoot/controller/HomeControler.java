package demo.example.DemoSpringBoot.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeControler {

	private String name = "Trần Tiến";
	
	@GetMapping("/home")
	private ResponseEntity<String> getData() {
		return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/home")
	private String addData(@RequestBody String msg) {
		System.out.print(msg);
		return msg;
	}

	@PutMapping("/home")
	private String updateData(@RequestBody String name) {
		this.name = name;
		return this.name;
	}
	
	@DeleteMapping("/home/{id}")
	private String deleteData(@PathVariable String id) {
		this.name = id;
		return this.name;
	}
}
