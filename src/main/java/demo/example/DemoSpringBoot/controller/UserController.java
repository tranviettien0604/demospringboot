package demo.example.DemoSpringBoot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import demo.example.DemoSpringBoot.entity.USERS;
import demo.example.DemoSpringBoot.service.UserService;

@RestController
public class UserController {

	@Autowired
	UserService userService;

	@GetMapping("/user")
	private ResponseEntity<List<USERS>> getUser() {
		return new ResponseEntity<List<USERS>>(userService.getUser(), HttpStatus.OK);
	}

	@PostMapping("/user")
	private ResponseEntity<?> addUser(@RequestBody USERS u) {
		try {
			return new ResponseEntity<>(userService.addUser(u), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Thêm user thất bại", HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/user/{id}")
	private ResponseEntity<?> delete(@PathVariable String id) {
		try {
			userService.delete(id);
			return new ResponseEntity<>("Xóa user" + id + "thành công", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Xóa user" + id + "thất bại", HttpStatus.BAD_REQUEST);
		}
	}
}
