package demo.example.DemoSpringBoot.repository;

import org.springframework.stereotype.Repository;

import demo.example.DemoSpringBoot.entity.USERS;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UserRepository extends JpaRepository<USERS, String>{

}
